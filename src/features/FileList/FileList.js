import React from 'react';
import PropTypes from 'prop-types';

const FileList = ({ files }) =>
  files
    ? files.map(({ id, label }) => (
        <div key={id}>
          <span>{id}</span>
          <span>{label}</span>
        </div>
      ))
    : 'no files here';

FileList.propTypes = {
  files: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      label: PropTypes.string.isRequired,
    }),
  ).isRequired,
};

export default FileList;
