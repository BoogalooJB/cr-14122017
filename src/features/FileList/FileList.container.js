import FileList from './FileList';
import { connect } from 'react-redux';
import { compose, lifecycle } from 'recompose';
import { loadFileList } from 'shared/actions';
const mapStateToProps = state => ({
  files: state.files,
});

const mapDispatchToProps = {
  loadFileList,
};

const enhance = compose(
  connect(mapStateToProps, mapDispatchToProps),
  lifecycle({
    componentDidMount() {
      // dispatch an action asking for files
      this.props.loadFileList();
    },
  }),
);

const FileListContainer = enhance(FileList);

FileListContainer.propTypes = {};

export default FileListContainer;
