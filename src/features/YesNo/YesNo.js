import React from 'react';
import cn from 'classnames';
import {
  compose,
  withStateHandlers,
  branch,
  renderComponent,
  withHandlers,
} from 'recompose';

import './yesNo.css';

const Yes = ({ nbClicks, active, onClick }) => (
  <div
    onClick={!active ? onClick : undefined}
    className={cn('yes', { active })}
  >
    {nbClicks}
  </div>
);

const No = ({ nbClicks, active, onClick }) => (
  <div onClick={!active ? onClick : undefined} className={cn('no', { active })}>
    {nbClicks}
  </div>
);

const YesNo = ({ value, pickYes, pickNo, counter }) => (
  <div className="yes-no">
    <Yes nbClicks={counter.yes} active={value === 'yes'} onClick={pickYes} />
    <No nbClicks={counter.no} active={value === 'no'} onClick={pickNo} />
  </div>
);

const logImTired = val => console.log(`You said \`${val}\` but I don’t care`);

const handleTiredness = withHandlers({
  pickYes: ({ pickYes }) => () =>
    Math.random() > 0.5 ? logImTired('yes') : pickYes(),
  pickNo: ({ pickNo }) => () =>
    Math.random() > 0.5 ? logImTired('no') : pickNo(),
});

const enhance = compose(
  withStateHandlers(
    { value: null, counter: { yes: 0, no: 0 } },
    {
      pickYes: ({ counter }) => () => ({
        value: 'yes',
        counter: { ...counter, yes: counter.yes + 1 },
      }),
      pickNo: ({ counter }) => () => ({
        value: 'no',
        counter: { ...counter, no: counter.no + 1 },
      }),
    },
  ),
  handleTiredness,
  branch(
    ({ counter }) => Math.max(counter.yes, counter.no) > 3,
    renderComponent(() => <div className="kidding">are you kidding me ?</div>),
  ),
);

export default enhance(YesNo);
