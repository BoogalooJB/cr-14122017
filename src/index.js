import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import configureStore from './configureStore';
// import FileList from './features/FileList/';
import YesNo from './features/YesNo/';
import './index.css';

const store = configureStore();

ReactDOM.render(
  <Provider store={store}>
    <YesNo value="yes" />
  </Provider>,
  document.getElementById('root'),
);
