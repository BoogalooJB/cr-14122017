import { LOAD_FILE_LIST, LOAD_FILE_LIST_SUCCESS } from 'shared/actions';

/*
{
  status: 'PENDING', // 'DONE'
  files: [{
      id: 'first-id',
      label: 'first file'
  }, {
      id: 'second-id',
      label: 'second file'
  }]
}
*/

const defaultState = {
  status: null,
  files: [],
};

export default (state = defaultState, action) => {
  switch (action.type) {
    case LOAD_FILE_LIST:
      return {
        ...state,
        status: 'PENDING',
      };
    case LOAD_FILE_LIST_SUCCESS:
      return {
        status: 'DONE',
        files: action.files,
      };
    default:
      return state;
  }
};
