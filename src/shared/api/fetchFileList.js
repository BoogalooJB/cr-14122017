const fileListSample = [
  {
    id: "first-file",
    label: "First file"
  },
  {
    id: "second-file",
    label: "Second file"
  },
  {
    id: "third-file",
    label: "Third file"
  }
];

export default () =>
  new Promise(resolve => setTimeout(() => resolve(fileListSample), 1000));
