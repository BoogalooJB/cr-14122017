import fetchFileList from 'shared/api/fetchFileList';
export const LOAD_FILE_LIST = 'LOAD_FILE_LIST';
export const LOAD_FILE_LIST_SUCCESS = 'LOAD_FILE_LIST_SUCCESS';

export const loadFileList = () => dispatch => {
  dispatch({
    type: LOAD_FILE_LIST,
  });
  return fetchFileList().then(files =>
    dispatch({
      type: LOAD_FILE_LIST_SUCCESS,
      files,
    }),
  );
};
